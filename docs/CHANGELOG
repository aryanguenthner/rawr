----- 0.21 -----

	BUGFIXES
		changed total_seconds() to seconds on line 584 in functions.py for compat. with 2.6 (CentOS/RedHat)
		install script - fixed a bug in kali detection
		fixed bug in dep detection - failed to exit
		fixed total columns in surface matrix

	CHANGES
		updated README to reflect addition of install.sh
		added to install.sh
			RedHat OS detection 
				** graphviz install is not detected by 'pip install pygraphviz - might need to work w/ env vars to make this happen **
			Version/Arch detection for RHEL and CentOS (to grab the correct version of epel
			added Fedora detection/install
			added nmap install
		made pygraphviz a non-essential module - added warning when not detected "diagrams not available"


----- 0.2 -----

	BUGFIXES
		no longer importing Ghost when not in use
		now initiating spider upon -S (--spider not required if using -S or --spider-opts
		diagrams not showing in html  ('m')

	CHANGES
		added install.sh
		added a null 'Accept-Encoding' header to disable compression
			re: https://github.com/ariya/phantomjs/issues/10930
		added title to meta html report


----- 0.1.91 -----

	BUGFIXES
		fixed HTML report madness - the one pixel space between the button and dropdown in FF on windows

	CHANGES
		pointed update function to latest phantomJS install
			https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-1.9.7...


----- 0.1.9 -----

	BUGFIXES
		removed 'cfgfile' setting when creating config file on current scan
		fixed a bug in glob_recurse recursion call
		fixed a bug in .cfg file creation - now excluding 'nmaprng' if False
		removed a line break that showed up even in --json and --json-min
		fixed bug in platform type check during tar function
		fixed bug in proxy usage due to update in requests module
		fixed bug in proxy error message
		fixed Bing Reverse DNS lookup
		fixed bug where PhantomJS was required during -m

	CHANGES
		changed '-b' to '--dns'
			placeholder code for DNS server axfr/queries
		started updating changelog by version rather than date
		implemented rawr_meta - now creates meta_report_<timestamp>.html if there are findings
		started pulling/storing CPE from NMap XML
		removed defpass.csv in favor of the DPE DB - http://www.toolswatch.org/dpe/  @ToolsWatch
			now matching CVE and default password info via CPE
		settings.py/use_ghost now used as switch for using phantomJS or ghost.py
		removed check for PhantomJS during update if use_ghost is True
		added break in Bing if "No Results Found" in return


----- 0.1.8 -----

	CHANGES
		removed '-n' and '-url' switches, now looking for the range/host/url as an arg
			ex:  ./rawr.py -x -o -r --spider http://canhazcode.com
			ex:  ./rawr.py google.com -x -o -r
			ex:  ./rawr.py 10.0.0.0/24 -x -o -r
		changed '--cfg' switch to '-c'
		edited/updated usage and help texts
		incremented to 0.1.80
		now pulling response content for each host (+spider) into a 'uniqued' wordlist
			<logfolder>/input_lists/wordlist_<host>_<port>_<ts>.lst
		updated regexes for modules, added a check for index pages
